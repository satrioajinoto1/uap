package Contest;

class PemesananTiket {
    // Do your magic here...
    private static TiketKonser[] tiketKonser;

    static {
        tiketKonser = new TiketKonser[]{
            new CAT8("CAT 8", 800000),
            new CAT1("CAT 1", 5000000),
            new FESTIVAL("FESTIVAL", 3500000),
            new VIP("VIP", 5700000),
            new VVIP("UNLIMITED EXPERIENCE", 11000000)
        };
    }

    public static TiketKonser pilihTiket(int index) {
        return tiketKonser[index];
    }
}