package Contest;

class FESTIVAL extends TiketKonser {
    //Do your magic here...
    // digunakan untuk mengakses parent class TiketConser
    public FESTIVAL(String nama, long harga) {
        super(nama, harga);
    }

    @Override
    // pembuatan ulang method dari TiketKonser untuk FESTIVAL 
    public long hitungHarga() {
        return harga;
    }
}