package Contest;

abstract class TiketKonser implements HargaTiket {
    // Do your magic here...
    protected String nama;
    protected long harga;

    public TiketKonser(String nama, long harga) {
        this.nama = nama;
        this.harga = harga;
    }

    public String getNama() {
        return nama;
    }

    public abstract long hitungHarga();
}