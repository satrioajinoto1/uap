package Contest;

class CAT1 extends TiketKonser {
    //Do your magic here...
    // digunakan untuk mengakses parent class TiketConser
    public CAT1(String nama, long harga) {
        super(nama, harga);
    }

    @Override
    // pembuatan ulang method dari TiketKonser untuk CAT1
    public long hitungHarga() {
        return harga;
    }
}
