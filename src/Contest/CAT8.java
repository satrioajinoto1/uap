package Contest;

class CAT8 extends TiketKonser {
    //Do your magic here...
    // digunakan untuk mengakses parent class TiketConser
    public CAT8(String nama, long harga) {
        super(nama, harga);
    }

    @Override
    // pembuatan ulang method dari TiketKonser untuk CAT8
    public long hitungHarga() {
        return harga;
    }
}