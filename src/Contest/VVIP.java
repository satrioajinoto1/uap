package Contest;

class VVIP extends TiketKonser {
    // Do your magic here...
    // digunakan untuk mengakses parent class TiketConser
    public VVIP(String nama, long harga) {
        super(nama, harga);
    }

    @Override
    // pembuatan ulang method dari TiketKonser untuk VVIP
    public long hitungHarga() {
        return harga;
    }
}