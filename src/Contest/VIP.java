package Contest;

class VIP extends TiketKonser {
    // Do your magic here...
    // digunakan untuk mengakses parent class TiketConser
    public VIP(String nama, long harga) {
        super(nama, harga);
    }

    @Override
    // pembuatan ulang method dari TiketKonser untuk VIP
    public long hitungHarga() {
        return harga;
    }
}